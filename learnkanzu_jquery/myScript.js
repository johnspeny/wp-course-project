$(document).ready(function () {
  // array to hold todo objects
  let todoContainer = [];

  // custom ordinal eg th, rd used on dates
  function dateOrdinal(dom) {
    if (dom == 31 || dom == 21 || dom == 1) return dom + "st";
    else if (dom == 22 || dom == 2) return dom + "nd";
    else if (dom == 23 || dom == 3) return dom + "rd";
    else return dom + "th";
  }

  // custom date formatter eg 23rd Apri 2020
  function formatDate(date, format) {
    const map = {
      mm: date.toLocaleString("default", { month: "short" }),
      dd: dateOrdinal(date.getDate()),
      yy: date.getFullYear(),
      yyyy: date.getFullYear(),
    };

    return format.replace(/mm|dd|yy|yyy/gi, (matched) => map[matched]);
  }

  // create todo items and store in todoContainer
  let id_num = 0;
  function addTodo(text) {
    // todo object
    const todo = {
      id: id_num++,
      text,
      checked: false,
      created: formatDate(new Date(Date.now()), "dd mm yy"),
    };
    // push todo object to todoContainer
    todoContainer.push(todo);
    displayTodo(todo);
  }

  // add event listener on add button of todo form
  // Add a submit event listener
  $("#form").on("submit", (event) => {
    // prevent page refresh on form submission
    event.preventDefault();

    // select the text input by name
    const input = $("input[name='todo']");

    // Get the value of the input and remove whitespace
    const text = input.val().trim();

    if (text !== "") {
      addTodo(text);
      input.val("");
      input.focus();
    }
  });

  function displayTodo(todo) {
    // Select the first element with a class of `todoDisplay`
    // We shall add todo lists to it
    const list = $(".todoDisplay").get(0);
    //const list = document.querySelector(".todoDisplay");
    //console.log(list);

    // select the current todo item in the DOM
    const item = document.querySelector(`[data-key='${todo.id}']`);

    // add this if block
    if (todo.deleted) {
      // remove the item from the DOM
      item.remove();
      return;
    }

    // Create an `li` element and assign it to `node`
    const node = document.createElement("li");

    // Set the class attribute
    node.setAttribute("class", `todo-item`);

    // Set the data-key attribute to the id of the todo
    node.setAttribute("data-key", todo.id);

    let rightDiv = "";
    let leftDiv = "";

    if (todo.checked) {
      // left side of todo item
      leftDiv = `
    <div class="task-left">
            <span>
                <i class="checkbox ${
                  todo.checked ? "fa fa-check-square-o" : "fa fa-square-o"
                }" aria-hidden="true"></i>
            </span>
            <input class="editable-input" type="text" value="${
              todo.text
            }" disabled />
        </div>
    `;

      // right side of todo items
      rightDiv = `
    <div class="task-right">
        <div class="task-right-action">
            <div>
                <span><i class="js-save-todo fa fa-check aria-hidden="true"></i></span>
                <span><i class="js-edit-todo fa fa-pencil" aria-hidden="true"></i></span>
                <span><i class="js-delete-todo fa fa-trash" aria-hidden="true"></i></span>
            </div>
        <div>
            <span><i class="fa fa-info-circle" aria-hidden="true"></i> ${todo.created}</span>
        </div>
        </div>
    </div>
    `;
    } else {
      leftDiv = `
    <div class="task-left">
        <span>
                <i class="checkbox ${
                  todo.checked ? "fa fa-check-square-o" : "fa fa-square-o"
                }" aria-hidden="true"></i>
        </span>
        <input type="text" value="${todo.text}" disabled />
        <span>
                <i class="fa fa-hourglass-half" aria-hidden="true"></i> ${
                  todo.created
                }
        </span>
    </div>
    `;
    }

    node.innerHTML = ` 
    <div class="task">
        ${leftDiv}
        ${rightDiv}
    </div>`;

    // If the item already exists in the DOM
    if (item) {
      list.replaceChild(node, item);
    } else {
      // otherwise append it to the end of the list
      list.append(node);
    }
  }

  // Mark as completed
  const list = document.querySelector(".js-todoDisplay");

  // Add a click event listener to the list and its children
  list.addEventListener("click", (event) => {
    if (event.target.classList.contains("checkbox")) {
      const itemKey = event.target.closest(".todo-item").dataset.key;
      toggleDone(itemKey);
    }

    if (event.target.classList.contains("js-delete-todo")) {
      const itemKey = event.target.closest(".todo-item").dataset.key;
      deleteTodo(itemKey);
    }

    if (event.target.classList.contains("js-edit-todo")) {
      let target = event.target.closest(".todo-item");
      const itemKey = event.target.closest(".todo-item").dataset.key;

      // change remove the disabled from input field
      target.getElementsByClassName("editable-input")[0].disabled = false;
      target.getElementsByClassName("editable-input")[0].focus();
    }

    if (event.target.classList.contains("js-save-todo")) {
      const itemKey = event.target.closest(".todo-item").dataset.key;

      // get the editable todo items
      const editForm = event.target
        .closest(".todo-item")
        .getElementsByClassName("editable-input")[0];

      // get the save icon and apply events on it
      const el = event.target
        .closest(".todo-item")
        .getElementsByClassName("js-save-todo")[0];

      // click on the save icon
      el.addEventListener("click", function () {
        let text = editForm.value;
        const index = todoContainer.findIndex(
          (item) => item.id === Number(itemKey)
        );

        if (text !== "") {
          const todo = {
            id: todoContainer[index].id,
            text: todoContainer[index].text,
            checked: todoContainer[index].checked,
            created: todoContainer[index].created,
          };

          // using spreads
          todoContainer = todoContainer.map((todo) => {
            return { ...todo };
          });
          todoContainer.find((todo) => todo.id == Number(itemKey)).text = text;

          //console.log(todoContainer[0]);

          displayTodo(todoContainer[0]);

          //editForm.value = "";
          editForm.disabled = true;
          //input.focus();
        }
      });
    }
  });

  function toggleDone(key) {
    // findIndex is an array method that returns the position of an element
    // in the array.
    const index = todoContainer.findIndex((item) => item.id === Number(key));
    // Locate the todo item in the todoItems array and set its checked
    // property to the opposite. That means, `true` will become `false` and vice
    // versa.
    todoContainer[index].checked = !todoContainer[index].checked;
    displayTodo(todoContainer[index]);
  }

  function deleteTodo(key) {
    // find the corresponding todo object in the todoItems array
    const index = todoContainer.findIndex((item) => item.id === Number(key));
    // Create a new object with properties of the current todo item
    // and a `deleted` property which is set to true
    const todo = {
      deleted: true,
      ...todoContainer[index],
    };
    // remove the todo item from the array by filtering it out
    todoContainer = todoContainer.filter((item) => item.id !== Number(key));
    displayTodo(todo);
  }
});
