<!DOCTYPE html>
<html lang="en">

<?php $title = 'Logged-in'; ?>
<?php include('header.php'); ?>

<body>

    <?php

    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        if (!empty($_POST["fname"]) || !empty($_POST["email"]))
        {
            $name = htmlspecialchars($_POST['fname']);
            echo "<p>Name: $name</p><br>";
            $email = htmlspecialchars($_POST['email']);
            echo "<p>Email: $email</p>";
        }
        else
        {
            $fieldErr = "Field is required";
            echo "fieldErr";
        }
    }

    ?>

    <?php include('footer.php'); ?>
</body>

</html>