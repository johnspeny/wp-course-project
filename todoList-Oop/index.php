<?php

require_once("Database.php");
require_once("TodoGateway.php");

// database setup
$conn = new Database("localhost", "wp_course", "root", "");
$conn->getConnection();

// to access the sql operations
$gateway = new TodoGateway($conn);

// insert data
if (!empty($_POST["todo"]))
{
  $data = array("todo" => $_POST["todo"]);
  $id = $gateway->create($data);
}

// display data
$new_array = $gateway->getAll();


// delete
if (isset($_GET['id']))
{
  $id = $_GET['id'];
  $id_new = $gateway->delete($id);
  header('Location: index.php');
  exit();
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
  <link rel="stylesheet" href="style.css" />
  <script src="myScript.js" defer></script>
  <title>My Todo-s</title>
</head>

<body>

  <h1></h1>

  <!-- card -->
  <div class="card">
    <!-- Heading -->
    <div class="heading">
      <span><i class="fa fa-check-square" aria-hidden="true"></i></span>
      <span>My Todo-s</span>
    </div>

    <!-- form -->
    <form id="form" method="post" action="/">
      <input type="text" name="todo" placeholder="Add new.." />
      <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
      <input type="submit" name="submit" value="Add" />
    </form>
    <!-- </div> -->

    <!-- separator -->
    <hr class="separator" />

    <!-- filters -->
    <div class="containerfilter">
      <div id="filter">
        <label for="all_select"> Filter </label>
        <select id="all_select">
          <option value="All">All</option>
        </select>
      </div>
      <div id="date">
        <label for="date_select"> Sort </label>
        <select id="date_select">
          <option value="Added date">Added date</option>
        </select>
        <span><i class="fa fa-sort-amount-asc" aria-hidden="true"></i></span>
      </div>
    </div>

    <!-- tasks -->
    <ul class="todoDisplay js-todoDisplay">
      <?php

      foreach ($new_array as $key => $value)
      {
        echo '
        <li>
            <div class="task">
            <div class="task-left">
          <span>
            <i class="checkbox ${
                todo.checked ? " fa fa-check-square-o" : "fa fa-square-o" }" aria-hidden="true"></i>
          </span>';
        echo '<input class="editable-input" type="text" value="';
        echo $value["title"];
        echo '" disabled />
        </div>';


        echo '<div class="task-right">
          <div class="task-right-action">
            <div>
            <span><button type="submit" id="myBtn"><i class="js-edit-todo fa fa-pencil" aria-hidden="true"></i></button></span>
              ';

        $del = <<<EOT
              <span><button type="submit" name="submit" onclick="javascript:location.href='index.php?id={$value["id"]}'"><i class="js-delete-todo fa fa-trash" aria-hidden="true"></i></button></span>
              </div>
              <div>
            EOT;

        echo $del;

        echo '<span><i class="fa fa-info-circle" aria-hidden="true"></i> ';
        echo $value["date_added"];
        echo '</span>
            </div>
          </div>
        </div>
    </li>';
      }

      ?>

    </ul>
  </div>

</body>

</html>