<!DOCTYPE html>
<html lang="en">

<?php $title = 'Login'; ?>
<?php include('header.php'); ?>

<body>
    <form action="/logged-in.php" method="post">
        <label for="fname">Name:</label><br>
        <input type="text" id="fname" name="fname" placeholder="jon"><br>
        <label for="email">Email:</label><br>
        <input type="text" id="email" name="email" placeholder="jon@email.com"><br><br>
        <input type="submit">
    </form>
    <?php include('footer.php'); ?>
</body>

</html>