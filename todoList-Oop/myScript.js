$(function () {
  $("#form").on("submit", function (e) {
    e.preventDefault();
    let myPromise = new Promise((resolve, reject) => {
      $.ajax({
        type: "post",
        url: "index.php",
        data: $("#form").serialize(),
        success: function (msg) {
          resolve(msg);
        },
        error: function (error) {
          reject(error);
        },
      });
    });

    myPromise
      .then((msg) => {
        $("body").html(msg)
      })
      .catch((error) => {
        console.log(error);
      });
  });
});
