<?php

/**
 * Summary of Database
 */
class Database
{
    //constructor property promotion and assign values to these properties
    public function __construct(
        private string $host,
        private string $name,
        private string $user,
        private string $password
    )
    {
    }

    public function getConnection(): PDO
    {
        $dns = "mysql:host={$this->host};dbname={$this->name}";
        // remove stringify in data attributes
        return new PDO($dns, $this->user, $this->password, [
            PDO::ATTR_EMULATE_PREPARES => false,
            PDO::ATTR_STRINGIFY_FETCHES => false
        ]);
    }
}
