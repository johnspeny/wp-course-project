<?php

class TodoGateway
{
    private PDO $conn;
    public function __construct(Database $database)
    {
        $this->conn = $database->getConnection();
    }

    public function getAll(): array
    {
        $sql = "SELECT * FROM to_do_list_items";
        $results = $this->conn->query($sql);
        $new_array = [];
        while ($row = $results->fetch(PDO::FETCH_ASSOC))
        {
            $new_array[] = $row; // append to new array
        }
        return $new_array;
    }

    public function create(array $data): string
    {
        $sql = "INSERT INTO to_do_list_items (title) VALUES (:title)";
        $stmt = $this->conn->prepare($sql);
        // bind the values to the placeholder
        $stmt->bindValue(":title", $data["todo"], PDO::PARAM_STR);
        // execute statement
        $stmt->execute();
        return $this->conn->lastInsertId();
    }

    public function delete(int $id): int
    {
        $sql = "DELETE FROM to_do_list_items WHERE id = :id";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->rowCount();
    }
}
