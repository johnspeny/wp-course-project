<?php

declare(strict_types=1); // strict types

$to_do_list = array(
    array(
        "id" => 0,
        "text" => "A",
        "checked" => false,
        "created" => "12312"
    ),
    array(
        "id" => 1,
        "text" => "B",
        "checked" => false,
        "created" => "12312"
    ),
    array(
        "id" => 2,
        "text" => "C",
        "checked" => false,
        "created" => "12312"
    ),
    array(
        "id" => 3,
        "text" => "D",
        "checked" => false,
        "created" => "12312"
    ),
    array(
        "id" => 4,
        "text" => "E",
        "checked" => false,
        "created" => "12312"
    )
);

// returns the to-do-list
function getTodoList(): array
{
    return $GLOBALS['to_do_list'];
}

// update todo lis
function updateTodoList(int $index, $item): array
{
    $GLOBALS['to_do_list'][$index] = $item;
    return $GLOBALS['to_do_list'];
}

// delete  
function deleteTodoListItem(int $index): array
{
    array_splice($GLOBALS['to_do_list'], $index, 1);
    return $GLOBALS['to_do_list'];
}

print_r(getTodoList());
print_r(deleteTodoListItem(0));
